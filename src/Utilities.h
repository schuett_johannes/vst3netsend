//
//  Utilities.h
//  VST3NetSend
//
//  Created by Vlad Gorloff on 07.01.13.
//  Copyright (c) 2013 Vlad Gorloff. All rights reserved.
//

#ifndef __VST3NetSend__Utilities__
#define __VST3NetSend__Utilities__

GV_NAMESPACE_BEGIN

std::string std_sprintf(const char* fmt, ...);

GV_NAMESPACE_END

#endif /* defined(__VST3NetSend__Utilities__) */
