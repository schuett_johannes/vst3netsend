//
//  GVIntegerFormatter.h
//  VST3NetSend
//
//  Created by Vlad Gorloff on 27.01.13.
//  Copyright (c) 2013 Vlad Gorloff. All rights reserved.
//

@interface GVIntegerFormatter : NSFormatter

-(NSString *) stringForNumberValue:(NSNumber*)anNumber;

@end
